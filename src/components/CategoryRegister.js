import React, { useState } from 'react'
import { Card, CardContent, Button, TextField } from '@mui/material'
import axios from 'axios'

function CategoryRegister(props) {
    const [nome, setNome]=useState("")
    const [descricao, setDescricao]=useState("")

    function registerCategoria(){
        axios.post("http://localhost:3001/salvarcategoria",
        {
            "nome":nome,
            "descricao":descricao
        }).then(
            r=>{alert("Categoria cadastrada com sucesso!")}
        )
    }
  return (
    <Card>
        <CardContent>
            <div style={{fontWeight:"bold", fontSize:'18px'}}>{props.texto}</div>
            <div style={{display:'flex', flexDirection:'column'}}>
                <div  style={{width:'60%', marginTop:'10px'}}>
                    <TextField value={nome} onChange={(e)=>{setNome(e.target.value)}} fullWidth id="outlined-basic" label="Categoria" variant="outlined" />
                </div>
                <div  style={{width:'60%', marginTop:'10px'}}>
                    <TextField value={descricao} onChange={(e)=>{setDescricao(e.target.value)}} fullWidth id="outlined-basic" label="Descrição" variant="outlined" />
                </div>
                <div style={{width:'60%', marginTop:'10px', display:'flex', justifyContent:'right'}}>
                    <Button variant="contained" onClick={()=>{registerCategoria()}}>Salvar</Button>
                </div>
            </div>
        </CardContent>
    </Card>
  )
}

export default CategoryRegister
