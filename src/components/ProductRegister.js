import { Card, CardContent, Button, TextField, FormControl, InputLabel, Select, MenuItem } from '@mui/material'
import axios from 'axios'
import React, { useState, useEffect } from 'react'

function ProductRegister(props) {

   const [nome,setNome] = useState("")
   const [preco,setPreco] = useState("")
   const [categoria,setCategoria] = useState("")
   const [categoriasOptions,setCategoriaOptions] = useState([])
   
   function registrarProduto(){
    axios.post("http://localhost:3001/salvarproduto",{
        
        "nome":nome,
        "preco":preco,
        "categoria":categoria
        
    }).then(
        r=>{alert("Produto cadastrado com sucesso!")}
    )
   }

   useEffect(()=>{
    axios.get("http://localhost:3001/listarcategorias").then(
    r=>{
        setCategoriaOptions(r.data.categorias)
    }
    )
},[])

  return (
    <Card>
        <CardContent>
            <div style={{fontWeight:"bold", fontSize:'18px'}}>{props.texto}</div>
            <div style={{display:'flex', flexDirection:'column'}}>
                <div  style={{width:'60%', marginTop:'10px'}}>
                    <TextField value={nome} onChange={(e)=>{setNome(e.target.value)}} fullWidth id="outlined-basic" label="Nome" variant="outlined" />
                </div>
                <div style={{width:'60%', marginTop:'10px'}}>
                    <TextField value={preco} onChange={(e)=>{setPreco(e.target.value)}} fullWidth id="outlined-basic" label="Preço" variant="outlined" />
                </div>
                <div style={{width:'60%', marginTop:'10px'}}>
                   {/*<TextField value={categoria} onChange={(e)=>{setCategoria(e.target.value)}} fullWidth id="outlined-basic" label="Categoria" variant="outlined" />*/}
                   <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">Categoria</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={categoria}
                        label="Age"
                        onChange={(e)=>{setCategoria(e.target.value)}}
                    >
                        {categoriasOptions.map(c=>(
                            <MenuItem value={c.id}>{c.nome}</MenuItem>
                        ))

                        }
                    </Select>
                    </FormControl>
                </div>
                <div style={{width:'60%', marginTop:'10px', display:'flex', justifyContent:'right'}}>
                    <Button variant="contained" onClick={()=>{registrarProduto()}}>Salvar</Button>
                </div>
            </div>

           
        </CardContent>
    </Card>
  )
}

export default ProductRegister
