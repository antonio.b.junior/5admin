import React, { useEffect, useState } from 'react'
import {  TableContainer,Table, TableHead, TableRow, TableCell, TableBody, Paper } from '@mui/material'
import axios from 'axios'


function ProductList(props) {
 const[rows,setrows]= useState([])

useEffect(()=>{
  axios.get("http://localhost:3001/listarprodutos").then(
  r=>{
    setrows(r.data.produtos)
  }

  )
},[])

/*
  const rows = [
    {
      id:1,
      nome:'Teclado',
      preco:100,
      categoria:'Informática'
    },
    {
      id:2,
      nome:'Monitor',
      preco:900,
      categoria:'Informática'
    },
    {
      id:3,
      nome:'Mouse',
      preco:40,
      categoria:'Informática'
    }
  ];
*/

  return (
    <div>
      <div>
        <h4>{props.texto}</h4>
      </div>
      <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
          <TableCell>ID</TableCell>
            <TableCell>Nome</TableCell>
            <TableCell align="right">Preço</TableCell>
            <TableCell align="right">Categoria</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.nome}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">{row.id}</TableCell>
              <TableCell component="th" scope="row">{row.produto}</TableCell>
              <TableCell align="right">R$ {row.preco}</TableCell>
              <TableCell align="right">{row.nome}</TableCell>

            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </div>
  )
}

export default ProductList
