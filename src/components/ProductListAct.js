import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { TextField, Button, Select, MenuItem, FormControl, InputLabel, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import {  TableContainer,Table, TableHead, TableRow, TableCell, TableBody, Paper, Divider } from '@mui/material'

const ProductApp = () => {
  const [products, setProducts] = useState([]);
  const [categoriasOptions,setCategoriaOptions] = useState([])
  const [newProduct, setNewProduct] = useState({
    nome: "",
    valor: "",
    categoria: ""
  });
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [openEditDialog, setOpenEditDialog] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);

  const [mantenedorasOptions, setMantenedoraOptions] = useState([])
  const [filiaisOptions, setFiliaisOptions] = useState([])
  const [selectedMantenedora, setSelectedMantenedora] = useState('');
  const [selectedFilial, setSelectedFilial] = useState('');

  const fetchProducts = async () => {
    const response = await axios.get("http://localhost:3001/listarprodutos"); 
    setProducts(response.data.produtos);
    console.log(response.data)
  };
  const fetchCategoriasOptions = async () => {
    const response = await axios.get("http://localhost:3001/listarcategorias");
    setCategoriaOptions(response.data.categorias);
    console.log(response.data)
  }; 

  const fetchMantenedorasOptions = async () => {
    const response = await axios.get("http://localhost:3001/listarmantenedoras");
    setMantenedoraOptions(response.data.mantenedoras);
    console.log(response.data)
  };

  const fetchFiliaisOptions = async () => {
    const response = await axios.get(`http://localhost:3001/listarfiliais/${selectedMantenedora}`);
    setFiliaisOptions(response.data.filiais);
    console.log(response.data)
  };

  useEffect(() => {
    fetchMantenedorasOptions();
  }, []);

  useEffect(() => {
    if (selectedMantenedora) {
      fetchFiliaisOptions();
    }
  }, [selectedMantenedora]);

  useEffect(() => {
    if (selectedMantenedora && selectedFilial) {
      fetchProducts();
      fetchCategoriasOptions();
    }
  }, [selectedMantenedora, selectedFilial]);

  /*
  useEffect(() => {
    fetchProducts();
    fetchCategoriasOptions();
  }, []);
*/
  
  const addProduct = async (e) => {
    e.preventDefault();
    const response = await axios.post("http://localhost:3001/salvarproduto", newProduct);
    setProducts([...products, response.data]);
    setNewProduct({ nome: "", preco: "", categoria: "" });
    fetchProducts();  // Atualiza a lista de produtos

  };

  const deleteProduct = async (id) => {
    await axios.delete(`http://localhost:3001/excluirproduto/${id}`);
    setProducts(products.filter(product => product.id !== id));
    setOpenDeleteDialog(false);
    fetchProducts();  // Atualiza a lista de produtos
  };

  const updateProduct = async () => {
    const productToUpdate = {
      ...selectedProduct, 
      categoria: selectedProduct.id_categoria
    };
    const response = await axios.put(`http://localhost:3001/updproduto/${selectedProduct.id}`, productToUpdate);
    setProducts(products.map(product => product.id === selectedProduct.id ? response.data : product));
    setOpenEditDialog(false);
    fetchProducts();  // Atualiza a lista de produtos
  };

  const handleChange = (e) => {
    setNewProduct({ ...newProduct, [e.target.name]: e.target.value });
  };

  const handleEditChange = (e) => {
    if(e.target.name === 'categoria') {
      setSelectedProduct({ ...selectedProduct, id_categoria: e.target.value });
    } else {
      setSelectedProduct({ ...selectedProduct, [e.target.name]: e.target.value });
    }
  };

  return (
    <div>
      <h1>Produtos</h1>
      <h2>Adicionar novo produto</h2>

      <div>

      <h2>Selecione a Mantenedora e FIlial</h2>
        <FormControl variant="outlined" style={{marginBottom: '20px', width:'200px', marginRight:'15px'}}>
          <InputLabel>Mantenedora</InputLabel>
          <Select
            label="Mantenedora"
            name="mantenedora"
            value={selectedMantenedora}
            onChange={(e) => setSelectedMantenedora(e.target.value)}
          >
            <MenuItem value="">
              <em>--</em>
            </MenuItem>
            {
              mantenedorasOptions.map(m => (
                <MenuItem value={m.id}>{m.nome_empresarial}</MenuItem>
              ))
            }
          </Select>
        </FormControl>

        <FormControl variant="outlined" style={{marginBottom: '20px', width:'200px', marginRight:'15px'}}>
          <InputLabel>Filial</InputLabel>
          <Select
            label="Filial"
            name='filial'
            value={selectedFilial}
            onChange={(e) => setSelectedFilial(e.target.value)}
          >
          <MenuItem value="">
              <em>--</em>
            </MenuItem>
            {filiaisOptions.map((option) => (
              <MenuItem key={option.id} value={option.id}>
                {option.nome_empresarial}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        </div>
    <Divider style={{marginBottom:'30px', marginTop:'20px'}} />
      <form onSubmit={addProduct}>
        <TextField 
          label="Nome do produto" 
          name="nome" 
          value={newProduct.nome}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px'}}
        />
        <TextField 
          label="Preço do produto" 
          name="preco"
          value={newProduct.preco}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px'}}
        />
        <FormControl variant="outlined" style={{marginBottom: '20px', width:'195px', marginRight:'20px'}}>
          <InputLabel>Categoria do produto</InputLabel>
          <Select
            label="Categoria do produto"
            name="categoria"
            value={newProduct.categoria}
            onChange={handleChange}
          >
            <MenuItem value="">
              <em>--</em>
            </MenuItem>
            {
                categoriasOptions.map(c=>(
                    <MenuItem value={c.id}>{c.nome}</MenuItem>
                ))

            }
          </Select>
        </FormControl>
        <Button type="submit" variant="contained" color="primary" style={{marginTop:'10px'}}>
          Adicionar produto
        </Button>
      </form>
      <h2>Produtos Cadastrados</h2>
      <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
                <TableRow>
                <TableCell>ID</TableCell>
                <TableCell>Nome</TableCell>
                <TableCell align="left">Valor</TableCell>
                <TableCell align="left">Categoria</TableCell>
                <TableCell align="left">Ações</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {products && products.map((product) => (
                <TableRow
                    key={product.produto}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                    <TableCell component="th" scope="row">{product.id}</TableCell>
                    <TableCell component="th" scope="row">{product.nome}</TableCell>
                    <TableCell align="left">R$ {product.valor}</TableCell>
                    <TableCell align="lrft">{product.categoria}</TableCell>
                    <TableCell align="lrft">
                        <Button color="primary" onClick={() => {setOpenEditDialog(true); setSelectedProduct(product);}}>Editar</Button>
                        <Button color="secondary" onClick={() => {setOpenDeleteDialog(true); setSelectedProduct(product);}}>Excluir</Button>
                    </TableCell>

                </TableRow>
                ))}
            </TableBody>
            </Table>
        </TableContainer>
{/*
      {products && products.map((product) => (
        <div key={product.id}>
          <h2>{product.nome}</h2>
          <p>{product.preco}</p>
          <p>{product.categoria}</p>
          <Button color="primary" onClick={() => {setOpenEditDialog(true); setSelectedProduct(product);}}>Editar</Button>
          <Button color="secondary" onClick={() => {setOpenDeleteDialog(true); setSelectedProduct(product);}}>Excluir</Button>
        </div>
      ))}
*/}
      <Dialog open={openDeleteDialog} onClose={() => setOpenDeleteDialog(false)}>
        <DialogTitle>Excluir Produto</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Você tem certeza que deseja excluir o produto {selectedProduct?.produto}?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpenDeleteDialog(false)}>Cancelar</Button>
          <Button onClick={() => deleteProduct(selectedProduct.id)} color="secondary">Excluir</Button>
        </DialogActions>
      </Dialog>
      <Dialog open={openEditDialog} onClose={() => setOpenEditDialog(false)}>
  <DialogTitle>Editar Produto</DialogTitle>
  <DialogContent>
    <TextField 
      label="Nome do produto" 
      name="nome"
      value={selectedProduct?.nome || ''} 
      onChange={handleEditChange}
      fullWidth
      margin="normal"
    />
    <TextField 
      label="Valor do produto" 
      name="valor"
      value={selectedProduct?.valor || ''}
      onChange={handleEditChange}
      fullWidth
      margin="normal"
    />
    <FormControl variant="outlined" style={{marginBottom: '20px', width:'200px', marginRight:'15px'}}>
      <InputLabel>Categoria do produto</InputLabel>
      <Select
        label="Categoria do produto"
        name="categoria"
        value={selectedProduct?.id_categoria || ''}
        onChange={handleEditChange}
      >
        <MenuItem value="">
          <em>--</em>
        </MenuItem>
        {
            categoriasOptions.map(c=>(
                <MenuItem value={c.id}>{c.nome}</MenuItem>
            ))
        }
      </Select>
    </FormControl>
  </DialogContent>
  <DialogActions>
    <Button onClick={() => setOpenEditDialog(false)}>Cancelar</Button>
    <Button onClick={updateProduct} color="primary">Atualizar</Button>
  </DialogActions>
</Dialog>


    </div>
  );
};

export default ProductApp;
