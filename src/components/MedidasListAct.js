import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { TextField, Button, Select, MenuItem, FormControl, InputLabel, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import {  TableContainer,Table, TableHead, TableRow, TableCell, TableBody, Paper, Divider } from '@mui/material'

const MedidasApp = () => {
  const [medidas, setMedidas] = useState([]);
  const [mantenedorasOptions, setMantenedoraOptions] = useState([])
  const [filiaisOptions, setFiliaisOptions] = useState([])
  const [selectedMantenedora, setSelectedMantenedora] = useState('');
  const [selectedFilial, setSelectedFilial] = useState('');
  const [newMedida, setNewMedida] = useState({
    nome: "",
    descricao: ""
  });
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [openEditDialog, setOpenEditDialog] = useState(false);
  const [selectedMedida, setSelectedUnidade] = useState(null);

  const fetchMedidas = async () => {
    const response = await axios.get(`http://localhost:3001/listarmedidas/${selectedMantenedora}/${selectedFilial}`); 
    setMedidas(response.data.medidas);
    console.log(response.data)
  };

  const fetchMantenedorasOptions = async () => {
    const response = await axios.get("http://localhost:3001/listarmantenedoras");
    setMantenedoraOptions(response.data.mantenedoras);
    console.log(response.data)
  };

  const fetchFiliaisOptions = async () => {
    const response = await axios.get(`http://localhost:3001/listarfiliais/${selectedMantenedora}`);
    setFiliaisOptions(response.data.filiais);
    console.log(response.data)
  };

  useEffect(() => {
    fetchMantenedorasOptions();
  }, []);

  useEffect(() => {
    if (selectedMantenedora) {
      fetchFiliaisOptions();
    }
  }, [selectedMantenedora]);

  useEffect(() => {
    if (selectedMantenedora && selectedFilial) {
      fetchMedidas();
    }
  }, [selectedMantenedora, selectedFilial]);

  const addUnidade = async (e) => {
    e.preventDefault();
    const response = await axios.post(`http://localhost:3001/salvarmedida/${selectedMantenedora}/${selectedFilial}`, newMedida);
    setMedidas([...medidas, response.data]);
    setNewMedida({ nome: "", descricao: "" });
    fetchMedidas();

  };
  
  const deleteUnidade = async (medida) => {
    await axios.delete(`http://localhost:3001/delmedida/${selectedMantenedora}/${selectedFilial}/${medida.id}`);
    setMedidas(medidas.filter((item) => item.id !== medida.id));
    setOpenDeleteDialog(false);
  };
  
  const updateUnidade = async () => {
    const response = await axios.put(`http://localhost:3001/alterarmedida/${selectedMantenedora}/${selectedFilial}/${selectedMedida.id}`, selectedMedida);
    setMedidas(medidas.map((medida) => (medida.id === selectedMedida.id ? response.data : medida)));
    setOpenEditDialog(false);
    fetchMedidas();
  };

return (
    <div>
        <h1>Medidas</h1>
    <div>

    <h2>Selecione a Mantenedora e FIlial</h2>
      <FormControl variant="outlined" style={{marginBottom: '20px', width:'200px', marginRight:'15px'}}>
        <InputLabel>Mantenedora</InputLabel>
        <Select
          label="Mantenedora"
          name="mantenedora"
          value={selectedMantenedora}
          onChange={(e) => setSelectedMantenedora(e.target.value)}
        >
          <MenuItem value="">
            <em>--</em>
          </MenuItem>
          {
            mantenedorasOptions.map(m => (
              <MenuItem value={m.id}>{m.nome_empresarial}</MenuItem>
            ))
          }
        </Select>
      </FormControl>
   
      <FormControl variant="outlined" style={{marginBottom: '20px', width:'200px', marginRight:'15px'}}>
        <InputLabel>Filial</InputLabel>
        <Select
          label="Filial"
          name='filial'
          value={selectedFilial}
          onChange={(e) => setSelectedFilial(e.target.value)}
        >
        <MenuItem value="">
            <em>--</em>
          </MenuItem>
          {filiaisOptions.map((option) => (
            <MenuItem key={option.id} value={option.id}>
              {option.nome_empresarial}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      </div>
        <Divider />
        <h2>Adicionar nova medida</h2>
      <form onSubmit={addUnidade}>
        <TextField
         variant="outlined"
          label="Nome"
          value={newMedida.nome}
          onChange={(e) => setNewMedida({ ...newMedida, nome: e.target.value })}
          style={{marginBottom: '20px', marginRight:'20px',  width:'20%'}}
        />
        <TextField
         variant="outlined"
          label="Descrição"
          value={newMedida.descricao}
          onChange={(e) => setNewMedida({ ...newMedida, descricao: e.target.value })}
          style={{marginBottom: '20px', marginRight:'20px',  width:'40%'}}
        />
        <Button type="submit" variant="contained" color="primary" style={{marginTop:'10px'}}>
            Adicionar Medida
        </Button>
      </form>
  
      <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Nome</TableCell>
              <TableCell>Descrição</TableCell>
              <TableCell>Ações</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {medidas.map((unidade) => (
              <TableRow 
              key={unidade.id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell component="th" scope="row">{unidade.nome}</TableCell>
                <TableCell component="th" scope="row">{unidade.descricao}</TableCell>
                <TableCell align="left">
                <Button color="primary" onClick={() => {setOpenEditDialog(true); setSelectedUnidade(unidade);}}>Editar</Button>
                <Button color="secundary" onClick={() => {setOpenDeleteDialog(true); setSelectedUnidade(unidade);}}>Excluir</Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
  
      {/* Dialog for editing unit */}
      <Dialog open={openEditDialog} onClose={() => setOpenEditDialog(false)}>
        <DialogTitle>Editar Medida</DialogTitle>
        <DialogContent>
          <TextField
            label="Nome"
            value={selectedMedida?.nome || ''}
            onChange={(e) => setSelectedUnidade({ ...selectedMedida, nome: e.target.value })}
            fullWidth
            margin="normal"
          />
          <TextField
            label="Descrição"
            value={selectedMedida?.descricao || ''}
            onChange={(e) => setSelectedUnidade({ ...selectedMedida, descricao: e.target.value })}
            fullWidth
            margin="normal"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpenEditDialog(false)}>Cancelar</Button>
          <Button onClick={updateUnidade}>Salvar</Button>
        </DialogActions>
      </Dialog>
  
      {/* Dialog for deleting unit */}
      <Dialog
        open={openDeleteDialog}
        onClose={() => setOpenDeleteDialog(false)}
      >
        <DialogTitle>Excluir Medida</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Você tem certeza que deseja deletar a medida {selectedMedida?.nome}?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpenDeleteDialog(false)}>Cancelar</Button>
          <Button onClick={() => deleteUnidade(selectedMedida)}>Excluir</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default MedidasApp;