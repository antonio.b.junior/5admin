import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { TextField, Button, Dialog, Select, MenuItem,  FormControl,InputLabel, DialogActions, DialogContent, DialogContentText, DialogTitle, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Paper, Divider } from '@mui/material';

const FilialApp = () => {
  const [mantenedoras, setMantenedoras] = useState([]);
  const [selectedMantenedora, setSelectedMantenedora] = useState("");
  const [selectedMantenedoraModal, setSelectedMantenedoraModal] = useState(null);

  //const [selectedMantenedora, setSelectedMantenedora] = useState(null);
  const [selectedFilial, setSelectedFilial] = useState(null);
  const [filiais, setFiliais] = useState([]);
  const [ufsOptions, setUfsOptions] = useState([]);
  const [newFilial, setNewFilial] = useState({
    nome_empresarial: "",
    nome_contato: "",
    cnpj: "",
    id_uf: "", 
    email_empresarial: "", 
    email_contato: "", 
    DDD_empresarial: "", 
    DDD_contato: "", 
    telefone_empresarial: "", 
    telefone_contato: "", 
    url_site: "", 
    cep: "", 
    endereco: "", 
    complemento: "", 
    bairro: "", 
    cidade: "",
    mantenedora_id: ""

  });
  //console.log(selectedMantenedora);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [openEditDialog, setOpenEditDialog] = useState(false);
  
  const fetchMantenedoras = async () => {
    const response = await axios.get("http://localhost:3001/listarmantenedoras");
    setMantenedoras(response.data.mantenedoras);
  };

  const fetchFiliais = async (mantenedoraId) => {
    if(!isNaN(parseInt(mantenedoraId))){
      const response = await axios.get(`http://localhost:3001/listarfiliais/${mantenedoraId}`);
      setFiliais(response.data.filiais);
    }
  };

  useEffect(() => {
    fetchFiliais();
    fetchUFsOptions();
    fetchMantenedoras();

  }, []);

  useEffect(() => {
    if (selectedMantenedora) {
      fetchFiliais(selectedMantenedora);
    }
  }, [selectedMantenedora]);

  const fetchUFsOptions = async () => {
    const response = await axios.get("http://localhost:3001/listarufs");
    setUfsOptions(response.data.ufs);
    console.log(response.data)
  }; 

  const addFilial = async (e) => {
    console.log(selectedMantenedora);
    e.preventDefault();
    
    const response = await axios.post(`http://localhost:3001/salvarfilial/${selectedMantenedora}`, newFilial);
    setFiliais([...filiais, response.data]);
    setNewFilial({ 
        nome_empresarial: "", nome_contato: "", cnpj: "", id_uf: "", email_empresarial: "", email_contato: "",
        DDD_empresarial: "", DDD_contato: "", telefone_empresarial: "", telefone_contato: "", url_site: "", cep: "", 
        endereco: "", complemento: "", bairro: "", cidade: "" 
     });
     fetchFiliais(selectedMantenedora);
     fetchUFsOptions();
    
  };

  const deleteFilial = async (id) => {
    await axios.delete(`http://localhost:3001/delfilial/${id}`);
    setFiliais(filiais.filter(filial => filial.id !== id));
    setOpenDeleteDialog(false);
    fetchFiliais(selectedMantenedora);
    fetchUFsOptions();
  };


  const updateFilial = async () => {
    const response = await axios.put(`http://localhost:3001/alterarfilial/${selectedFilial.id}/${selectedMantenedora}/${selectedMantenedoraModal}`, selectedFilial); 
    setFiliais(filiais.map(filial => filial.id === selectedFilial.id ? response.data : filial));
    setOpenEditDialog(false);
    fetchFiliais(selectedMantenedora);
    fetchUFsOptions();
  };


  const handleChange = (e) => {
    setNewFilial({ ...newFilial, [e.target.name]: e.target.value , mantenedora_id: selectedMantenedora });
  };

  const handleEditChange = (e) => {
    setSelectedFilial({ ...selectedFilial, [e.target.name]: e.target.value });
    if(e.target.name === "mantenedora"){
      setSelectedMantenedoraModal(e.target.value)
    }
  };


return (
    <div>
      <h1>Filiais</h1>
      <h2>Selecione a Mantenedora</h2>
      <FormControl variant="outlined" style={{marginBottom: '20px', width:'200px', marginRight:'15px'}}>
        <InputLabel>Mantenedora</InputLabel>
        <Select
          label="Mantenedora"
          name="mantenedora"
          value={selectedMantenedora}
          onChange={(e) => setSelectedMantenedora(e.target.value)}
        >
          <MenuItem value="">
            <em>--</em>
          </MenuItem>
          {
            mantenedoras.map(m => (
              <MenuItem value={m.id}>{m.nome_empresarial}</MenuItem>
            ))
          }
        </Select>
      </FormControl>

      <h2>Adicionar nova Filial</h2>
      <Divider />
      <h3>Dados da Filial</h3>
      <form onSubmit={addFilial}>
      
      <div>
        <TextField 
                label="Nome" 
                name="nome_empresarial" 
                value={newFilial.nome_empresarial}
                onChange={handleChange}
                variant="outlined"
                style={{marginBottom: '20px', marginRight:'20px', width:'65%' }}
                />

            <TextField 
            label="CNPJ" 
            name="cnpj" 
            value={newFilial.cnpj}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'30%'}}
            />
      </div>
    <div>
        <TextField 
            label="Email" 
            name="email_empresarial" 
            value={newFilial.email_empresarial}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'53%' }}
            />
            <TextField 
            label="DDD" 
            name="DDD_empresarial" 
            value={newFilial.DDD_empresarial}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'10%' }}
            />
            <TextField 
            label="Telefone" 
            name="telefone_empresarial" 
            value={newFilial.telefone_empresarial}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'30%' }}
            />
    </div>
    <div>
    <TextField 
          label="Endereço" 
          name="endereco" 
          value={newFilial.endereco}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px', width:'50%'  }}
        />
    <TextField 
          label="CEP" 
          name="cep" 
          value={newFilial.cep}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px', width:'13%'  }}
        />
       <TextField 
          label="Complemento" 
          name="complemento" 
          value={newFilial.complemento}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px', width:'30%' }}
        />
    </div>
    <div>
       <TextField 
          label="Bairro" 
          name="bairro" 
          value={newFilial.bairro}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'23px', width:'35%' }}
        />
       <TextField 
          label="Cidade" 
          name="cidade" 
          value={newFilial.cidade}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'26px', width:'35%' }}
        />
        <FormControl variant="outlined" style={{marginBottom: '20px', width:'22%'}}>
          <InputLabel>UF</InputLabel>
          <Select
            label="Ufs"
            name="id_uf"
            value={newFilial.id_uf}
            onChange={handleChange}
          >
            <MenuItem value="">
              <em>--</em>
            </MenuItem>
            {
                ufsOptions.map(c=>(
                    <MenuItem value={c.id}>{c.nome}</MenuItem>
                ))

            }
          </Select>
        </FormControl>
       <TextField 
          label="URL Site (ex.: http://www.seusite.com.br)" 
          name="url_site" 
          value={newFilial.url_site}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px' , width:'40%' }}
        />
    </div>
    <Divider />
      <h3>Dados do Contato</h3>
      <div>
        <TextField 
            label="Nome" 
            name="nome_contato" 
            value={newFilial.nome_contato}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px' , width:'65%' }}
            />
  
        <TextField 
          label="Email" 
          name="email_contato" 
          value={newFilial.email_contato}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px' , width:'30%' }}
        />
        </div>
        <div>
        <TextField 
          label="DDD" 
          name="DDD_contato" 
          value={newFilial.DDD_contato}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px',  width:'10%' }}
        />
       <TextField 
          label="Telefone" 
          name="telefone_contato" 
          value={newFilial.telefone_contato}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px', width:'30%' }}
        />
        </div>
        <Button type="submit" variant="contained" color="primary" style={{marginBottom: '20px', marginTop:'10px'}}>
          Adicionar
        </Button>
      </form>
      <Divider />
      <h2>Lista de Filiais</h2>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
            <TableCell>ID</TableCell>
              <TableCell>Nome</TableCell>
              <TableCell>Contato</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>CNPJ</TableCell>
              <TableCell>Matenedora</TableCell>
              <TableCell>Ações</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {filiais.map((filial) => (
              <TableRow key={filial.id}>
                <TableCell>{filial.id}</TableCell>
                <TableCell>{filial.nome_empresarial}</TableCell>
                <TableCell>{filial.nome_contato}</TableCell>
                <TableCell>{filial.email_empresarial}</TableCell>
                <TableCell>{filial.cnpj}</TableCell>
                <TableCell>{filial.mantenedora}</TableCell>
                <TableCell>
                  <Button 
                    
                    color="primary"
                    onClick={() => {
                      setOpenEditDialog(true); 
                      setSelectedFilial(filial);
                      setSelectedMantenedoraModal(filial.id_mantenedora);
                    }}
                  >
                    Editar
                  </Button>

                  <Button 
                    
                    color="secondary"
                    onClick={() => {
                      setSelectedFilial(filial);
                      setOpenDeleteDialog(true);
                    }}
                  >
                    Excluir
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

            <Dialog open={openDeleteDialog} onClose={() => setOpenDeleteDialog(false)}>
            <DialogTitle>Excluir Filial</DialogTitle>
            <DialogContent>
                <DialogContentText>
                Você tem certeza que deseja excluir a filial {selectedFilial?.nome_empresarial}?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setOpenDeleteDialog(false)}>Cancelar</Button>
                <Button onClick={() => deleteFilial(selectedFilial.id)} color="secondary">Excluir</Button>
            </DialogActions>
            </Dialog>

            <Dialog open={openEditDialog} onClose={() => setOpenEditDialog(false)}   PaperProps={{ sx: { width: "100%", height: "80%" } }}>
            <DialogTitle>Editar Filial</DialogTitle>
            <DialogContent >
                <Divider />
                <h3>Dados da Filial</h3>
                <h2>Selecione a Mantenedora</h2>
                <FormControl variant="outlined" style={{marginBottom: '20px', width:'200px', marginRight:'15px'}}>
                  <InputLabel>Mantenedora da filial</InputLabel>
                  <Select
                    label="Mantenedora da filial"
                    name="mantenedora"
                    value={selectedMantenedoraModal || ''}
                    onChange={handleEditChange}
                  >
                    <MenuItem value="">
                      <em>--</em>
                    </MenuItem>
                    {
                        mantenedoras.map(m=>(
                            <MenuItem value={m.id}>{m.nome_empresarial}</MenuItem>
                        ))
                    }
                  </Select>
                </FormControl>

                <div>
                    <TextField 
                    label="Nome Empresarial" 
                    name="nome_empresarial"
                    value={selectedFilial?.nome_empresarial || ''} 
                    onChange={handleEditChange}
                    style={{marginBottom: '20px', marginRight:'20px', width:'100%' }}
                    margin="normal"
                    />
                    <TextField 
                    label="Nome do Contato" 
                    name="nome_contato"
                    value={selectedFilial?.nome_contato || ''}
                    onChange={handleEditChange}
                    style={{marginBottom: '20px', marginRight:'20px', width:'100%' }}
                    margin="normal"
                    />
                </div>
                <div>
                <TextField 
                label="CNPJ" 
                name="cnpj"
                value={selectedFilial?.cnpj || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', marginRight:'20px', width:'48%' }}
                margin="normal"
                />
                <TextField 
                label="Email" 
                name="email_empresarial"
                value={selectedFilial?.email_empresarial || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', width:'48%' }}
                margin="normal"
                />
                </div>
                <div>
                <TextField 
                label="Endereço" 
                name="endereco"
                value={selectedFilial?.endereco || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', marginRight:'20px', width:'70%' }}
                margin="normal"
                />
                <TextField 
                label="CEP" 
                name="cep"
                value={selectedFilial?.cep || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', width:'25%' }}
                margin="normal"
                />
                </div>
                <div>
                <TextField 
                label="Complemento" 
                name="complemento"
                value={selectedFilial?.complemento || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px',marginRight:'20px', width:'47%' }}
                margin="normal"
                />
                <TextField 
                label="Bairro" 
                name="bairro"
                value={selectedFilial?.bairro || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', width:'48%' }}
                margin="normal"
                />
                </div>
                <div>
                <TextField 
                label="Endereço" 
                name="endereco" 
                value={selectedFilial?.endereco}
                onChange={handleEditChange}
                variant="outlined"
                style={{marginBottom: '20px', marginRight:'10px', width:'100%' }}
                />
                </div>
                <div>
                <TextField 
                label="cidade" 
                name="cidade"
                value={selectedFilial?.cidade || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', marginTop: '5px',marginRight:'20px', width:'47%' }}
                margin="normal"
                />
                <FormControl variant="outlined" style={{marginBottom: '20px', marginTop: '5px', width:'48%'}}>
                <InputLabel>UF</InputLabel>
                <Select
                    label="Ufs"
                    name="id_uf"
                    value={selectedFilial?.id_uf}
                    onChange={handleEditChange}
                >
                    <MenuItem value="">
                    <em>--</em>
                    </MenuItem>
                    {
                        ufsOptions.map(c=>(
                            <MenuItem value={c.id}>{c.nome}</MenuItem>
                        ))

                    }
                </Select>
                </FormControl>
                </div>
                <div>
                <TextField 
                label="URL Site (ex.: http://www.seusite.com.br)" 
                name="url_site" 
                value={selectedFilial?.url_site}
                onChange={handleEditChange}
                variant="outlined"
                style={{marginBottom: '20px', marginRight:'20px' , width:'40%' }}
                />
                </div>
                <Divider />
                <h3>Dados do Contato</h3>
                <div>
                <TextField 
                    label="Nome" 
                    name="nome_contato" 
                    value={selectedFilial?.nome_contato}
                    onChange={handleEditChange}
                    variant="outlined"
                    style={{marginBottom: '20px', marginRight:'20px' , width:'100%' }}
                    />
                 </div>
                <div>       
                <TextField 
                label="Email" 
                name="email_contato" 
                value={selectedFilial?.email_contato}
                onChange={handleEditChange}
                variant="outlined"
                style={{marginBottom: '20px', marginRight:'20px', width:'50%' }}
                />

                <TextField 
                label="DDD" 
                name="DDD_contato" 
                value={selectedFilial?.DDD_contato}
                onChange={handleEditChange}
                variant="outlined"
                style={{marginBottom: '20px', marginRight:'10px',  width:'10%' }}
                />
            <TextField 
                label="Telefone" 
                name="telefone_contato" 
                value={selectedFilial?.telefone_contato}
                onChange={handleEditChange}
                variant="outlined"
                style={{marginBottom: '20px', width:'33.8%' }}
                />
                </div>

            </DialogContent>
            <DialogActions>
                <Button onClick={() => setOpenEditDialog(false)}>Cancelar</Button>
                <Button onClick={updateFilial} color="primary">Atualizar</Button>
            </DialogActions>
            </Dialog>
      
    </div>
  );
};

export default FilialApp;
