import React, { useState, useEffect } from 'react';
import axios from 'axios';

import { TextField, Button, Dialog, Select, MenuItem,  FormControl,InputLabel, DialogActions, DialogContent, DialogContentText, DialogTitle, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Paper, Divider } from '@mui/material';

const MantenedoraApp = () => {
  const [mantenedoras, setMantenedoras] = useState([]);
  const [ufsOptions, setUfsOptions] = useState([]);
  const [newMantenedora, setNewMantenedora] = useState({
    nome_empresarial: "",
    nome_contato: "",
    cnpj: "",
    id_uf: "", 
    email_empresarial: "", 
    email_contato: "", 
    DDD_empresarial: "", 
    DDD_contato: "", 
    telefone_empresarial: "", 
    telefone_contato: "", 
    url_site: "", 
    cep: "", 
    endereco: "", 
    complemento: "", 
    bairro: "", 
    cidade: ""
  });
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [openEditDialog, setOpenEditDialog] = useState(false);
  const [selectedMantenedora, setSelectedMantenedora] = useState(null);

  const fetchMantenedoras = async () => {
    const response = await axios.get("http://localhost:3001/listarmantenedoras");
    setMantenedoras(response.data.mantenedoras);
  };

  useEffect(() => {
    fetchMantenedoras();
    fetchUFsOptions();
  }, []);


  const fetchUFsOptions = async () => {
    const response = await axios.get("http://localhost:3001/listarufs");
    setUfsOptions(response.data.ufs);
    console.log(response.data)
  }; 

  const addMantenedora = async (e) => {
    e.preventDefault();
    const response = await axios.post("http://localhost:3001/salvarmantenedora", newMantenedora);
    setMantenedoras([...mantenedoras, response.data]);
    setNewMantenedora({ 
        nome_empresarial: "", nome_contato: "", cnpj: "", id_uf: "", email_empresarial: "", email_contato: "",
        DDD_empresarial: "", DDD_contato: "", telefone_empresarial: "", telefone_contato: "", url_site: "", cep: "", 
        endereco: "", complemento: "", bairro: "", cidade: "" 
     });
    fetchMantenedoras();
    fetchUFsOptions();
  };

  const deleteMantenedora = async (id) => {
    await axios.delete(`http://localhost:3001/delmantenedora/${id}`);
    setMantenedoras(mantenedoras.filter(mantenedora => mantenedora.id !== id));
    setOpenDeleteDialog(false);
    fetchMantenedoras(); 
    fetchUFsOptions();
  };

  const updateMantenedora = async () => {
    const response = await axios.put(`http://localhost:3001/alterarmantenedora/${selectedMantenedora.id}`, selectedMantenedora);
    setMantenedoras(mantenedoras.map(mantenedora => mantenedora.id === selectedMantenedora.id ? response.data : mantenedora));
    setOpenEditDialog(false);
    fetchMantenedoras();
    fetchUFsOptions();
  };

  const handleChange = (e) => {
    setNewMantenedora({ ...newMantenedora, [e.target.name]: e.target.value });
  };

  const handleEditChange = (e) => {
    setSelectedMantenedora({ ...selectedMantenedora, [e.target.name]: e.target.value });
  };


return (
    <div>
      <h1>Mantenedoras</h1>
      <h2>Adicionar nova mantenedora</h2>
      <Divider />
      <h3>Dados da Mantenedora</h3>
      <form onSubmit={addMantenedora}>
      
      <div>
        <TextField 
                label="Nome" 
                name="nome_empresarial" 
                value={newMantenedora.nome_empresarial}
                onChange={handleChange}
                variant="outlined"
                style={{marginBottom: '20px', marginRight:'20px', width:'65%' }}
                />

            <TextField 
            label="CNPJ" 
            name="cnpj" 
            value={newMantenedora.cnpj}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'30%'}}
            />
      </div>
    <div>
        <TextField 
            label="Email" 
            name="email_empresarial" 
            value={newMantenedora.email_empresarial}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'53%' }}
            />
            <TextField 
            label="DDD" 
            name="DDD_empresarial" 
            value={newMantenedora.DDD_empresarial}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'10%' }}
            />
            <TextField 
            label="Telefone" 
            name="telefone_empresarial" 
            value={newMantenedora.telefone_empresarial}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'30%' }}
            />
    </div>
    <div>
    <TextField 
          label="Endereço" 
          name="endereco" 
          value={newMantenedora.endereco}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px', width:'50%'  }}
        />
    <TextField 
          label="CEP" 
          name="cep" 
          value={newMantenedora.cep}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px', width:'13%'  }}
        />
       <TextField 
          label="Complemento" 
          name="complemento" 
          value={newMantenedora.complemento}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px', width:'30%' }}
        />
    </div>
    <div>
       <TextField 
          label="Bairro" 
          name="bairro" 
          value={newMantenedora.bairro}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'23px', width:'35%' }}
        />
       <TextField 
          label="Cidade" 
          name="cidade" 
          value={newMantenedora.cidade}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'26px', width:'35%' }}
        />


        <FormControl variant="outlined" style={{marginBottom: '20px', width:'22%'}}>
          <InputLabel>UF</InputLabel>
          <Select
            label="Ufs"
            name="id_uf"
            value={newMantenedora.id_uf}
            onChange={handleChange}
          >
            <MenuItem value="">
              <em>--</em>
            </MenuItem>
            {
                ufsOptions.map(c=>(
                    <MenuItem value={c.id}>{c.nome}</MenuItem>
                ))

            }
          </Select>
        </FormControl>
       <TextField 
          label="URL Site (ex.: http://www.seusite.com.br)" 
          name="url_site" 
          value={newMantenedora.url_site}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px' , width:'40%' }}
        />
    </div>
    <Divider />
      <h3>Dados do Contato</h3>
      <div>
        <TextField 
            label="Nome" 
            name="nome_contato" 
            value={newMantenedora.nome_contato}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px' , width:'65%' }}
            />
  
        <TextField 
          label="Email" 
          name="email_contato" 
          value={newMantenedora.email_contato}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px' , width:'30%' }}
        />
        </div>
        <div>
        <TextField 
          label="DDD" 
          name="DDD_contato" 
          value={newMantenedora.DDD_contato}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px',  width:'10%' }}
        />
       <TextField 
          label="Telefone" 
          name="telefone_contato" 
          value={newMantenedora.telefone_contato}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px', width:'30%' }}
        />
        </div>
        <Button type="submit" variant="contained" color="primary" style={{marginTop:'10px'}}>
          Adicionar
        </Button>
      </form>
      <h2>Lista de mantenedoras</h2>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
            <TableCell>ID</TableCell>
              <TableCell>Nome Empresarial</TableCell>
              <TableCell>Nome do Contato</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>CNPJ</TableCell>
              <TableCell>Ações</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {mantenedoras.map((mantenedora) => (
              <TableRow key={mantenedora.id}>
                <TableCell>{mantenedora.id}</TableCell>
                <TableCell>{mantenedora.nome_empresarial}</TableCell>
                <TableCell>{mantenedora.nome_contato}</TableCell>
                <TableCell>{mantenedora.email_empresarial}</TableCell>
                <TableCell>{mantenedora.cnpj}</TableCell>
                {/* Adicione mais células de dados aqui */}
                <TableCell>
                  <Button 
                    
                    color="primary"
                    onClick={() => {
                      setSelectedMantenedora(mantenedora);
                      setOpenEditDialog(true);
                    }}
                  >
                    Editar
                  </Button>

                  <Button 
                    
                    color="secondary"
                    onClick={() => {
                      setSelectedMantenedora(mantenedora);
                      setOpenDeleteDialog(true);
                    }}
                  >
                    Excluir
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

            <Dialog open={openDeleteDialog} onClose={() => setOpenDeleteDialog(false)}>
            <DialogTitle>Excluir Mantenedora</DialogTitle>
            <DialogContent>
                <DialogContentText>
                Você tem certeza que deseja excluir a mantenedora {selectedMantenedora?.nome_empresarial}?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setOpenDeleteDialog(false)}>Cancelar</Button>
                <Button onClick={() => deleteMantenedora(selectedMantenedora.id)} color="secondary">Excluir</Button>
            </DialogActions>
            </Dialog>

            <Dialog open={openEditDialog} onClose={() => setOpenEditDialog(false)}   PaperProps={{ sx: { width: "100%", height: "80%" } }}>
            <DialogTitle>Editar Mantenedora</DialogTitle>
            <DialogContent >
                <Divider />
                <h3>Dados da Mantenedora</h3>
                <div>
                    <TextField 
                    label="Nome Empresarial" 
                    name="nome_empresarial"
                    value={selectedMantenedora?.nome_empresarial || ''} 
                    onChange={handleEditChange}
                    style={{marginBottom: '20px', marginRight:'20px', width:'100%' }}
                    margin="normal"
                    />
                    <TextField 
                    label="Nome do Contato" 
                    name="nome_contato"
                    value={selectedMantenedora?.nome_contato || ''}
                    onChange={handleEditChange}
                    style={{marginBottom: '20px', marginRight:'20px', width:'100%' }}
                    margin="normal"
                    />
                </div>
                <div>
                <TextField 
                label="CNPJ" 
                name="cnpj"
                value={selectedMantenedora?.cnpj || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', marginRight:'20px', width:'48%' }}
                margin="normal"
                />
                <TextField 
                label="Email" 
                name="email_empresarial"
                value={selectedMantenedora?.email_empresarial || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', width:'48%' }}
                margin="normal"
                />
                </div>
                <div>
                <TextField 
                label="Endereço" 
                name="endereco"
                value={selectedMantenedora?.endereco || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', marginRight:'20px', width:'70%' }}
                margin="normal"
                />
                <TextField 
                label="CEP" 
                name="cep"
                value={selectedMantenedora?.cep || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', width:'25%' }}
                margin="normal"
                />
                </div>
                <div>
                <TextField 
                label="Complemento" 
                name="complemento"
                value={selectedMantenedora?.complemento || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px',marginRight:'20px', width:'47%' }}
                margin="normal"
                />
                <TextField 
                label="Bairro" 
                name="bairro"
                value={selectedMantenedora?.bairro || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', width:'48%' }}
                margin="normal"
                />
                </div>
                <div>
                <TextField 
                label="Endereço" 
                name="endereco" 
                value={selectedMantenedora?.endereco}
                onChange={handleEditChange}
                variant="outlined"
                style={{marginBottom: '20px', marginRight:'10px', width:'100%' }}
                />
                </div>
                <div>
                <TextField 
                label="cidade" 
                name="cidade"
                value={selectedMantenedora?.cidade || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', marginTop: '5px',marginRight:'20px', width:'47%' }}
                margin="normal"
                />
                <FormControl variant="outlined" style={{marginBottom: '20px', marginTop: '5px', width:'48%'}}>
                <InputLabel>UF</InputLabel>
                <Select
                    label="Ufs"
                    name="id_uf"
                    value={selectedMantenedora?.id_uf}
                    onChange={handleEditChange}
                >
                    <MenuItem value="">
                    <em>--</em>
                    </MenuItem>
                    {
                        ufsOptions.map(c=>(
                            <MenuItem value={c.id}>{c.nome}</MenuItem>
                        ))

                    }
                </Select>
                </FormControl>
                </div>
                <div>
                <TextField 
                label="URL Site (ex.: http://www.seusite.com.br)" 
                name="url_site" 
                value={selectedMantenedora?.url_site}
                onChange={handleEditChange}
                variant="outlined"
                style={{marginBottom: '20px', marginRight:'20px' , width:'40%' }}
                />
                </div>
                <Divider />
                <h3>Dados do Contato</h3>
                <div>
                <TextField 
                    label="Nome" 
                    name="nome_contato" 
                    value={selectedMantenedora?.nome_contato}
                    onChange={handleEditChange}
                    variant="outlined"
                    style={{marginBottom: '20px', marginRight:'20px' , width:'100%' }}
                    />
                 </div>
                <div>       
                <TextField 
                label="Email" 
                name="email_contato" 
                value={selectedMantenedora?.email_contato}
                onChange={handleEditChange}
                variant="outlined"
                style={{marginBottom: '20px', marginRight:'20px', width:'50%' }}
                />

                <TextField 
                label="DDD" 
                name="DDD_contato" 
                value={selectedMantenedora?.DDD_contato}
                onChange={handleEditChange}
                variant="outlined"
                style={{marginBottom: '20px', marginRight:'10px',  width:'10%' }}
                />
            <TextField 
                label="Telefone" 
                name="telefone_contato" 
                value={selectedMantenedora?.telefone_contato}
                onChange={handleEditChange}
                variant="outlined"
                style={{marginBottom: '20px', width:'33.8%' }}
                />
                </div>

            </DialogContent>
            <DialogActions>
                <Button onClick={() => setOpenEditDialog(false)}>Cancelar</Button>
                <Button onClick={updateMantenedora} color="primary">Atualizar</Button>
            </DialogActions>
            </Dialog>
      
    </div>
  );
};

export default MantenedoraApp;
