import React, { useEffect, useState } from 'react';
import {  TableContainer,Table, TableHead, TableRow, TableCell, TableBody, Paper } from '@mui/material'
import axios from 'axios';


function CategoryList(props) {
const [rows, setRows]=useState([])

useEffect(()=>{
  axios.get("http://localhost:3001/listarcategorias").then(
    r=>{
      setRows(r.data.categorias)
    }
  )
},[])

/*
const rows = [
  {
    id:1,
    nome:'Informática'
  },
  {
    id:2,
    nome:'Papelaria'
  },
  {
    id:3,
    nome:'Suprimentos'
  }
];
*/

  return (
    <div>
      <div>
        <h4>{props.texto}</h4>
      </div>
      <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell>Nome</TableCell>
            <TableCell>Descrição</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">{row.id}</TableCell>
              <TableCell component="th" scope="row">{row.nome}</TableCell>
              <TableCell component="th" scope="row">{row.descricao}</TableCell>

            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </div>
  )
}


export default CategoryList
