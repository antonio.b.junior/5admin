import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { TextField, Button, Select, MenuItem, FormControl, InputLabel, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider } from '@material-ui/core';
import { TableContainer,Table, TableHead, TableRow, TableCell, TableBody, Paper } from '@mui/material'
import { Delete, AddBox, Edit, AddCircleOutlined,RemoveCircleOutlined } from '@mui/icons-material';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';

import { format } from 'date-fns';
import dayjs from 'dayjs';



//dayjs.locale('pt-BR') // use Spanish locale globally

const ComboApp = () => {
    const [combos, setCombos] = useState([]);
    const [mantenedorasOptions, setMantenedoraOptions] = useState([])
    const [filiaisOptions, setFiliaisOptions] = useState([])
    const [selectedMantenedora, setSelectedMantenedora] = useState('');
    const [selectedFilial, setSelectedFilial] = useState('');
    const [newCombo, setNewCombo] = useState({
      nome: "",
      descricao: "",
      valor:"", 
      perc_desconto:"",
      mostrar_valor:"", 
      dt_inicio:"", 
      dt_fim:""
    });
    const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
    const [openEditDialog, setOpenEditDialog] = useState(false);
    const [selectedCombo, setSelectedCombo] = useState(null);
    
    const [products, setProducts] = useState([]);
    const [comboProducts, setComboProducts] = useState([]);
    
    // Estado para armazenar o id do combo que está sendo editado
    const [editingComboId, setEditingComboId] = useState(null);
    //const [open, setOpen] = useState(false);
    const [selectedComboProducts, setSelectedComboProducts] = useState([]);
    const [editComboProducts, setEditComboProducts] = useState([]);
    const [editCombo, setEditCombo] = useState(null);

    

    
    const fetchProducts = async () => {
      const response = await axios.get(`http://localhost:3001/listarprodutos/${selectedMantenedora}/${selectedFilial}`);
      setProducts(response.data.produtos);
    };
    const fetchCombos = async () => {
      const response = await axios.get(`http://localhost:3001/listarcombos/${selectedMantenedora}/${selectedFilial}`);
      setCombos(response.data.combos);
    };
    
    const fetchFiliaisOptions = async () => {
      const response = await axios.get(`http://localhost:3001/listarfiliais/${selectedMantenedora}`);
      setFiliaisOptions(response.data.filiais);
      console.log(response.data)
    };

    const fetchMantenedorasOptions = async () => {
      const response = await axios.get("http://localhost:3001/listarmantenedoras");
      setMantenedoraOptions(response.data.mantenedoras);
      console.log(response.data)
    };

    useEffect(() => {  
      
      fetchMantenedorasOptions();
    }, []);
    
    useEffect(() => {
      if (selectedMantenedora) {
        fetchFiliaisOptions();
      }
      if (selectedMantenedora && selectedFilial) {
        fetchProducts();
        fetchCombos();
      }
    }, [selectedMantenedora, selectedFilial]);

    useEffect(() => {
      console.log('editComboProducts', editComboProducts);
    }, [editComboProducts]);
    
/* tela de trás */



    const handleIncreaseQuantity = (index) => {
      setComboProducts(comboProducts.map((product, i) => {
        if (i === index) {
          return { ...product, quantidade: product.quantidade + 1 };
        }
        return product;
      }));
    };
    
    const handleDecreaseQuantity = (index) => {
      setComboProducts(comboProducts.map((product, i) => {
        if (i === index && product.quantidade > 1) {
          return { ...product, quantidade: product.quantidade - 1 };
        }
        return product;
      }));
    };

    const handleAddProductToCombo = (product) => {
      // setComboProducts([...comboProducts, product]);
       setComboProducts([...comboProducts, { ...product, quantidade: 1 }]);
       setProducts(products.filter((p) => p.id !== product.id));
     };
     
 
     const handleRemoveProductFromCombo = (product) => {
      //comentário
       setProducts([...products, product]);
       setComboProducts(comboProducts.filter((p) => p.id !== product.id));
     };

/* modal */

const handleSelRemoveProductFromCombo = (product, index) => {
  console.log(product.nome_produto)
  const newComboProducts = [...editComboProducts];
  newComboProducts.splice(index, 1);
  setEditComboProducts(newComboProducts);

  // Adiciona o produto de volta à lista de produtos disponíveis
  const updatedSelectedProducts = [...selectedComboProducts];
  updatedSelectedProducts.push({
      id: product.id_produto,
      nome_produto: product.nome_produto,
      quantidade: 1
  });
  setSelectedComboProducts(updatedSelectedProducts);
};



    const handleSelAddProductToCombo = (product) => {
          // Adiciona o produto à lista de produtos do combo
          setEditComboProducts([...editComboProducts, {...product, quantidade: 1}]);

          // Remove o produto da lista de produtos disponíveis
          const newComboProducts = selectedComboProducts.filter(p => p.id_produto !== product.id_produto);
          setSelectedComboProducts(newComboProducts);
  }

    const handleIncreaseProductQuantity = (index) => {

      setEditComboProducts(editComboProducts.map((product, i) => {
        if (i === index) {
          return { ...product, quantidade: product.quantidade + 1 };
        }
        return product;
      }));
    };

    const handleDecreaseProductQuantity = (index) => {
      setEditComboProducts(editComboProducts.map((product, i) => {
        if (i === index && product.quantidade > 1) {
          return { ...product, quantidade: product.quantidade - 1 };
        }
        return product;
      }));
    };

    
 

const handleComboClick = async (comboId) => {

  // Recupera os produtos do combo com a nova query
  const response = await axios.get(`http://localhost:3001/combo/${selectedMantenedora}/${selectedFilial}/${comboId}`);
  const products = response.data.combo;
  console.log(products)
  //console.log(comboId)

  setEditingComboId(comboId);


  const comboProducts = [];
  const editComboProducts = [];

  let combo = null;

  // Separa os produtos que estão no combo e os que não estão
  products.forEach(product => {
    if (product.id_combo) {
      editComboProducts.push(product);
      // Assumindo que as informações do combo estão disponíveis nos produtos
      // do combo, atualizamos o objeto combo
      combo = { 
        id: product.id_combo,
        descricao: product.descricao_combo,
        dt_inicio: product.dt_inicio,
        dt_fim: product.dt_fim,
        valor: product.valor,
        mostrar_valor: product.mostrar_valor,
        nome_combo: product.nome_combo,
        perc_desconto: product.perc_desconto
      };
    } else {
      comboProducts.push(product);
    }
  });
//console.log(combo)

  setEditCombo(combo);
  setSelectedComboProducts(comboProducts);
  setEditComboProducts(editComboProducts);
  console.log('editComboProducts', editComboProducts); // Verificar o conteúdo de editComboProducts

  setOpenEditDialog(true);
};

  // Deletar combo
  const deleteCombo = async () => {
    // Deletar o combo da tabela tb_combos
    await axios.delete(`http://localhost:3001/deletarcombo/${selectedCombo.id}`);
    
    // Deletar todos os produtos do combo na tabela tb_combos_produtos
    comboProducts.forEach(async (product) => {
      await axios.delete(`http://localhost:3001/deletarcomboproduto/${selectedCombo.id}`);
    });

    // Atualizar a lista de combos
    setCombos(combos.filter((item) => item.id !== selectedCombo.id));
    setOpenDeleteDialog(false);
  };

    // Função chamada quando o usuário clica em "Salvar" na modal em modo de edição
    const handleUpdateCombo = async () => {
      // Atualizar o combo
      //await axios.put(`http://localhost:3001/atualizarcombo/${selectedMantenedora}/${selectedFilial}/${editingComboId}`, editCombo);
      const response = await axios.put(`http://localhost:3001/atualizarcombo/${selectedMantenedora}/${selectedFilial}/${editingComboId}`, editCombo);

      console.log(response.data.msg); // log the response data to console
      //console.log('editComboProducts', editComboProducts); // Verificar o conteúdo de editComboProducts

      //console.log(editingComboId); 
      if(response.data.msg==="OK"){
          // Atualizar os produtos do combo

          let novosDados = editComboProducts.map(product => ({
            id_combo: editingComboId,
            id_produto: product.id_produto,
            quantidade: product.quantidade,
            id_mantenedora: selectedMantenedora,
            id_filial: selectedFilial
          }));
          const response2 =   await axios.put(`http://localhost:3001/atualizarcomboproduto/${selectedMantenedora}/${selectedFilial}/${editingComboId}`, novosDados);
          if(response2==="OK"){
            console.log("gravou tudo")
          }
      }



      setComboProducts([]);
      setEditingComboId(null);
      setEditComboProducts([]);

      // Fechar a modal e recarregar os combos
      //setOpen(false);
      fetchCombos();
    };  

    const handleOpenEditDialog = (combo) => {
      setSelectedCombo(combo);
      handleComboClick(combo.id);
      setEditComboProducts(comboProducts);
      setOpenEditDialog(true);
    };
    


  // Salvar combo
  const saveCombo = async (e) => {
    e.preventDefault();
    // Salvar o combo na tabela tb_combos
    try{
      const response = await axios.post(`http://localhost:3001/salvarcombo/${selectedMantenedora}/${selectedFilial}`, newCombo);
      setCombos([...combos, response.data]);
      setNewCombo({ nome: "", descricao: "", valor:"", perc_desconto:"",mostrar_valor:"", dt_inicio:"", dt_fim:""});
    
      if (response.status === 200) {
        const combo = response.data;
        
        // Para cada produto no combo, salvar na tabela tb_combos_produtos
        comboProducts.forEach(async (product) => {
          await axios.post(`http://localhost:3001/salvarcomboproduto/${selectedMantenedora}/${selectedFilial}`, {
            id_combo: combo.id_combo,
            id_produto: product.id,
            quantidade: product.quantidade,
          });
        });
        // Atualizar a lista de combos
        fetchCombos();
      } else {
        // Lidar com erro aqui. Talvez mostrar uma mensagem de erro para o usuário
      }
    }
    catch (error) {
      console.error(error);
      // Lidar com erro aqui. Talvez mostrar uma mensagem de erro para o usuário
    }
  };
  
  const updateCombo = (field, value) => {
    setEditCombo(prevCombo => ({
      ...prevCombo,
      [field]: value
    }));
  };

    
  return (
    
    <div>
      
      <h1>Combos</h1>
      <div>
        <FormControl variant="outlined" style={{marginBottom: '20px', width:'200px', marginRight:'15px'}}>
          <InputLabel id="mantenedora-label">Mantenedora</InputLabel>
          <Select
            labelId="mantenedora-label"
            value={selectedMantenedora}
            
            onChange={(e) => setSelectedMantenedora(e.target.value)}
          >
            {mantenedorasOptions.map((man) => (
              <MenuItem key={man.id} value={man.id}>
                {man.nome_empresarial}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <FormControl variant="outlined" style={{marginBottom: '20px', width:'200px', marginRight:'15px'}}>
          <InputLabel id="filial-label">Filial</InputLabel>
          <Select
            labelId="filial-label"
            value={selectedFilial}
            onChange={(e) => setSelectedFilial(e.target.value)}
          >
            <MenuItem value="">
              <em>--</em>
            </MenuItem>
            {filiaisOptions.map((option) => (
              <MenuItem key={option.id} value={option.id}>
                {option.nome_empresarial}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
      
      <Divider style={{marginBottom: '20px', marginTop:'20px'}} />
      <h3>Dados do Combo</h3>
      <form onSubmit={saveCombo}>
       <div>
          <TextField
            label="Nome"
            variant="outlined"
            value={newCombo.nome}
            style={{marginBottom: '20px', marginRight:'20px', width:'40%' }}
            onChange={(e) => setNewCombo({ ...newCombo, nome: e.target.value })}
          />
          <TextField
            label="Descrição"
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'40%' }}
            value={newCombo.descricao}
            onChange={(e) => setNewCombo({ ...newCombo, descricao: e.target.value })}
          />
        </div>
       <div>
          <TextField
            label="% de desconto"
            variant="outlined"
            value={newCombo.perc_desconto}
            style={{marginBottom: '20px', marginRight:'20px', width:'40%' }}
            onChange={(e) => setNewCombo({ ...newCombo, perc_desconto: e.target.value })}
          />
          <TextField
            label="Valor"
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'18.3%' }}
            value={newCombo.valor}
            onChange={(e) => setNewCombo({ ...newCombo, valor: e.target.value })}
          />
          <TextField
            label="Mostra Valor"
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'20%' }}
            value={newCombo.mostrar_valor}
            onChange={(e) => setNewCombo({ ...newCombo, mostrar_valor: e.target.value })}
          />
        </div>
        <div style={{display:'flex'}}>
          <div style={{marginBottom: '20px', marginRight:'15px',  width:'25%' }}>
            <LocalizationProvider dateAdapter={AdapterDayjs}  adapterLocale="pt-br">
              
                <DateTimePicker
                value={newCombo.dt_inicio}
                label="Inicia em" 
                onChange={(value) => setNewCombo({ ...newCombo, dt_inicio: value })}
                renderInput={(params) => <TextField {...params} />}
                />
              
            </LocalizationProvider>
          </div>
          <div style={{marginBottom: '20px',  width:'25%' }} >
            <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="pt-br">
              
                <DateTimePicker
                
                value={newCombo.dt_fim}
                label="Finaliza em"
                onChange={(value) => setNewCombo({ ...newCombo, dt_fim: value })}
                renderInput={(params) => <TextField {...params} />}


                />
              
            </LocalizationProvider>
          </div>
        </div>
        
        <Divider style={{marginBottom: '20px', marginTop:'50px'}} />
        <h3>Itens do Combo</h3>
        <div style={{width:'100%',  marginBottom: '20px', marginTop:'20px'}}>
          <div style={{display: 'inline-block',  width:'45%', height:'100%', top:'0px', verticalAlign: 'top', maxHeight: '500px', overflow: 'auto'}}>
              <h2>Produtos disponíveis</h2>
              {products.map((product) => (
              <div key={product.id}>
                  <Button color="primary"  onClick={() => handleAddProductToCombo(product)}><AddBox /></Button>
                  {product.nome}

              </div>
              ))}
          </div>   
          
          <div style={{display: 'inline-block',  width:'45%', height:'100%', top:'0px', verticalAlign: 'top', maxHeight: '500px', overflow: 'auto'}}>
              <h2>Produtos no combo</h2>
              {comboProducts.map((product, index) => (
              <div key={product.id}>
                  <Button color="secundary"  onClick={() => handleRemoveProductFromCombo(product)}><Delete /></Button>
                  <Button color="primary"  onClick={() => handleIncreaseQuantity(index)}><AddCircleOutlined /></Button>
                  <span>{product.quantidade}</span>
                  <Button color="secundary"  onClick={() => handleDecreaseQuantity(index)}><RemoveCircleOutlined /></Button>
                  {product.nome}

              </div>
              ))}
          </div>
        </div>
        

        <Button  color="primary" variant="contained" type="submit">Salvar Combo</Button>
        
      </form>
      
      <Divider style={{marginBottom: '20px', marginTop:'50px'}} />
      <h3>Lista de Combos Cadastrados</h3>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Nome</TableCell>
              <TableCell>Descrição</TableCell>
              <TableCell>Valor</TableCell>
              <TableCell>% Desc</TableCell>
              <TableCell>Inicio</TableCell>
              <TableCell>Fim</TableCell>
              <TableCell>Ações</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {combos.map((combo) => (
              <TableRow key={combo.id}>
                <TableCell>{combo.nome}</TableCell>
                <TableCell>{combo.nome}</TableCell>
                <TableCell>{combo.descricao}</TableCell>
                <TableCell>{combo.valor}</TableCell>
                <TableCell>{combo.per_desconto}</TableCell>
                <TableCell>{format(new Date(combo.dt_inicio), 'dd/MM/yyyy HH:mm:ss')}</TableCell> 
                <TableCell>{format(new Date(combo.dt_fim), 'dd/MM/yyyy HH:mm:ss')}</TableCell>

                <TableCell>
                {/*<Button onClick={() => {setOpenEditDialog(true); setSelectedCombo(combo);handleComboClick(combo.id)}}><Edit /></Button>*/}
                <Button onClick={() => handleOpenEditDialog(combo)}><Edit /></Button>

                  <Button onClick={() => {setOpenDeleteDialog(true); setSelectedCombo(combo);}}><Delete /></Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    

    
  <Dialog open={openEditDialog} onClose={() => setOpenEditDialog(false)} aria-labelledby="form-dialog-title">
    <DialogTitle id="form-dialog-title">Editar combo</DialogTitle>
    <DialogContent>
      <DialogContentText>
        Para editar o combo, por favor, altere as informações desejadas.
      </DialogContentText>
      <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', marginBottom:'30px'}}>
      <TextField
        autoFocus
        margin="dense"
        id="nome"
        label="Nome do combo"
        type="text"
        fullWidth
        value={editCombo?.nome_combo || ''}
        onChange={(event) => updateCombo('nome_combo', event.target.value)}
      />
      <TextField
        autoFocus
        margin="dense"
        id="descricao"
        label="Descrição do combo"
        type="text"
        fullWidth
        value={editCombo?.descricao || ''}
        onChange={(event) => updateCombo('descricao', event.target.value)}
      />
      {
      console.log(editCombo?.dt_inicio ? dayjs(editCombo.dt_inicio).format() : null)
      }
      </div>
      <div>
          <TextField
            label="% de desconto"
            value={editCombo?.perc_desconto ||''}
            style={{marginBottom: '20px', marginRight:'20px', width:'40%' }}
            onChange={(event) => updateCombo('perc_desconto', event.target.value)}
          />
          <TextField
            label="Valor"
            style={{marginBottom: '20px', marginRight:'20px', width:'18.3%' }}
            value={editCombo?.valor}
            onChange={(event) => updateCombo('valor', event.target.value)}
          />
          <TextField
            label="Mostra Valor"
            style={{marginBottom: '20px', marginRight:'20px', width:'20%' }}
            value={editCombo?.mostrar_valor}
            onChange={(event) => updateCombo('mostrar_valor', event.target.value)}
          />
        </div>
      <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
        
      <LocalizationProvider dateAdapter={AdapterDayjs} locale="pt-br">
        <DateTimePicker
          label="Data e hora de início"
          inputVariant="outlined"
          value={editCombo?.dt_inicio ? dayjs(editCombo.dt_inicio) : null}
          onChange={(date) => updateCombo('dt_inicio', dayjs(date).toISOString())}
          ampm={false}
          format="DD/MM/YYYY HH:mm"
        />
      </LocalizationProvider>
      <LocalizationProvider dateAdapter={AdapterDayjs} locale="pt-br">
        <DateTimePicker
          label="Data e hora de fim"
          inputVariant="outlined"
          value={editCombo?.dt_fim ? dayjs(editCombo.dt_fim) : null}
          onChange={(date) => updateCombo('dt_fim', dayjs(date).toISOString())}
          ampm={false}
          format="DD/MM/YYYY HH:mm"
        />
      </LocalizationProvider>

      </div>
      <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignContent: 'flex-start', height: '300px', marginTop: '20px', overflowY: 'scroll'}}>
          <div style={{display: 'inline-block', border:'2px solid blue', width:'45%', marginRight:'10px'}}>
              <h2>Produtos disponíveis</h2>
              {selectedComboProducts.map((product) => (
                  <div key={product.id}>
                    <Button color="primary"  onClick={() => handleSelAddProductToCombo(product)}><AddBox /></Button>
                  {product.nome_produto}
                  
                  </div>
              ))}
          </div>    
          <div style={{display: 'inline-block', border:'2px solid green', width:'45%'}}>
          <h2>Produtos no combo</h2>
          {editComboProducts.map((product, index) => (
              <div key={product.id}>
              {product.nome_produto} - Quantidade: {product.quantidade}
              <div>
                  <Button color="secondary"  onClick={() => handleSelRemoveProductFromCombo(product, index)}><Delete /></Button>
                  <Button color="primary"  onClick={() => handleIncreaseProductQuantity(index)}><AddCircleOutlined /></Button>
                  <Button color="secondary"  onClick={() => handleDecreaseProductQuantity(index)}><RemoveCircleOutlined /></Button>
              </div> 
              </div>
          ))}
          </div>
      </div>

  </DialogContent>
  <DialogActions>
  <Button onClick={() => setOpenEditDialog(false)} color="primary">

      Cancelar
    </Button>
    <Button onClick={handleUpdateCombo} color="primary">
      Salvar
    </Button>
  </DialogActions>
</Dialog>


      {/* Dialog for deleting combo */}
      <Dialog
        open={openDeleteDialog}
        onClose={() => setOpenDeleteDialog(false)}
      >
        <DialogTitle>Deletar Combo</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Você tem certeza que deseja deletar este combo?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpenDeleteDialog(false)}>Cancelar</Button>
          <Button onClick={() => deleteCombo(selectedCombo)}>Deletar</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default ComboApp;
