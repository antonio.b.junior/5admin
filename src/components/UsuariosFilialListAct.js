import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { TextField, Button, Dialog, Select, MenuItem,  FormControl,InputLabel, DialogActions, DialogContent, DialogContentText, DialogTitle, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Paper, Divider } from '@mui/material';

const UsuarioFilialApp = () => {
  const [mantenedorasOptions, setMantenedoraOptions] = useState([])
  const [selectedMantenedora, setSelectedMantenedora] = useState("");
  const [selectedMantenedoraModal, setSelectedMantenedoraModal] = useState(null);
  const [selectedFilialModal, setSelectedFilialModal] = useState(null);
  const [filiaisOptions, setFiliaisOptions] = useState([])
  const [usuariosfiliais, setUsuariosFIliais]= useState([])

  //const [selectedMantenedora, setSelectedMantenedora] = useState(null);
  const [selectedFilial, setSelectedFilial] = useState(null);
  const [selectedUsuarioFilial, setSelectedUsuarioFilial] = useState(null);
  const [filiais, setFiliais] = useState([]);
  const [perfisOptions, setPerfisOptions] = useState([]);
  const [newUsuarioFilial, setNewUsuarioFilial] = useState({
    nome: "",
    cpf: "",
    email: "", 
    ddd: "", 
    telefone: "", 
    senha: "", 
    id_mantenedora: "", 
    id_filial: "",
    id_perfil:""

  });
  //console.log(selectedMantenedora);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [openEditDialog, setOpenEditDialog] = useState(false);
  
  const fetchMantenedorasOptions = async () => {
    const response = await axios.get("http://localhost:3001/listarmantenedoras");
    setMantenedoraOptions(response.data.mantenedoras);
    //console.log(response.data)
  };

  const fetchFiliaisOptions = async () => {
    const response = await axios.get(`http://localhost:3001/listarfiliais/${selectedMantenedora}`);
    setFiliaisOptions(response.data.filiais);
    //console.log(response.data)
  };

  useEffect(() => {  
    
    fetchMantenedorasOptions();
  }, []);

  const fetchFiliais = async (mantenedoraId) => {
    if(!isNaN(parseInt(mantenedoraId))){
      const response = await axios.get(`http://localhost:3001/listarfiliais/${mantenedoraId}`);
      setFiliais(response.data.filiais);
    }
  };

  const fetchUsuariosFiliais = async () => {
    const response = await axios.get(`http://localhost:3001/listarusuariosfilial/${selectedMantenedora}/${selectedFilial}`);
    //console.log(response.data.usuarios_filial)
    setUsuariosFIliais(response.data.usuarios_filial);
  };

  useEffect(() => {
    fetchFiliais();
    fetchPerfisOptions();
    fetchMantenedorasOptions();
    //fetchUsuariosFiliais();

  }, []);

  useEffect(() => {
    if (selectedMantenedora) {
      fetchFiliais(selectedMantenedora);
    }
  }, [selectedMantenedora]);

  useEffect(() => {
    if (selectedMantenedora) {
      fetchFiliaisOptions();
    }
    if (selectedMantenedora && selectedFilial) {
      fetchUsuariosFiliais();
    }
  }, [selectedMantenedora, selectedFilial]);

  const fetchPerfisOptions = async () => {
    const response = await axios.get("http://localhost:3001/listarperfis");
    setPerfisOptions(response.data.perfis);
    //console.log(response.data)
  }; 

  const addUsuarioFilial = async (e) => {
    //console.log(selectedMantenedora); 
    //console.log(newUsuarioFilial)
    e.preventDefault();
    
    const response = await axios.post(`http://localhost:3001/salvarusuariofilial/${selectedMantenedora}/${selectedFilial}`, newUsuarioFilial);
    setUsuariosFIliais([...usuariosfiliais, response.data]);
    setNewUsuarioFilial({ 
      nome: "",
      cpf: "",
      email: "", 
      ddd: "", 
      telefone: "", 
      senha: "", 
      id_mantenedora: "", 
      id_filial: "",
      id_perfil:""
     });
     
     fetchFiliais(selectedMantenedora);
     fetchUsuariosFiliais();
     fetchPerfisOptions();
    
  };

  const deleteFilial = async (id) => {
    await axios.delete(`http://localhost:3001/delfilial/${id}`);
    setFiliais(filiais.filter(filial => filial.id !== id));
    setOpenDeleteDialog(false);
    fetchFiliais(selectedMantenedora);
    fetchPerfisOptions();
  };


  const updateUsuarioFilial = async () => {
    console.log(selectedUsuarioFilial)
    const response = await axios.put(`http://localhost:3001/alterarusuariofilial/${selectedMantenedoraModal}/${selectedFilialModal}/${selectedUsuarioFilial.id}`, selectedUsuarioFilial); 
    setUsuariosFIliais(usuariosfiliais.map(usuario => usuario.id === selectedUsuarioFilial.id ? response.data : usuario));
    setOpenEditDialog(false);
    fetchFiliais(selectedMantenedora);
    fetchPerfisOptions();
    fetchUsuariosFiliais();
    
  };

/*
  const handleChange = (e) => {
    setNewUsuarioFilial({ ...newUsuarioFilial, [e.target.name]: e.target.value , id_mantenedora: selectedMantenedora, id_filial: selectedFilial });
  };
*/
  const handleChange = (e) => {
    setNewUsuarioFilial({ ...newUsuarioFilial, [e.target.name]: e.target.value , id_mantenedora: selectedMantenedora, id_filial: selectedFilial});
  };

  const handleEditChange = (e) => {
    //setSelectedFilial({ ...selectedFilial, [e.target.name]: e.target.value });
    //setSelectedUnidade({ ...selectedMedida, nome: e.target.value })};
    setSelectedUsuarioFilial({...selectedUsuarioFilial,[e.target.name]:e.target.value})
  };
  //console.log(usuariosfiliais)

return (
    <div>
      <h1>Usuários</h1>
      <h2>Selecione a Mantenedora</h2>
      <FormControl variant="outlined" style={{marginBottom: '20px', width:'200px', marginRight:'15px'}}>
        <InputLabel>Mantenedora</InputLabel>
        <Select
          label="Mantenedora"
          name="mantenedora"
          value={selectedMantenedora}
          onChange={(e) => setSelectedMantenedora(e.target.value)}
        >
          <MenuItem value="">
            <em>--</em>
          </MenuItem>
          {
            mantenedorasOptions.map(m => (
              <MenuItem value={m.id}>{m.nome_empresarial}</MenuItem>
            ))
          }
        </Select>
      </FormControl>

      <FormControl variant="outlined" style={{marginBottom: '20px', width:'200px', marginRight:'15px'}}>
          <InputLabel>Filial</InputLabel>
          <Select
            label="Filial"
            name="filial"
            value={selectedFilial}
            onChange={(e) => setSelectedFilial(e.target.value)}
          >
            <MenuItem value="">
              <em>--</em>
            </MenuItem>
            {filiaisOptions.map((option) => (
              <MenuItem key={option.id} value={option.id}>
                {option.nome_empresarial}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

      <h2>Adicionar novo usuário</h2>
      <Divider />
      <h3>Dados do Usuário</h3>
      <form onSubmit={addUsuarioFilial}>
      
      <div>
        <TextField 
                label="Nome" 
                name="nome" 
                value={newUsuarioFilial.nome}
                onChange={handleChange}
                variant="outlined"
                style={{marginBottom: '20px', marginRight:'20px', width:'65%' }}
                />

            <TextField 
            label="CPF" 
            name="cpf" 
            value={newUsuarioFilial.cpf}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'30%'}}
            />
      </div>
    <div>
        <TextField 
            label="Email" 
            name="email" 
            value={newUsuarioFilial.email}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'52.5%' }}
            />
            <TextField 
            label="ddd" 
            name="ddd" 
            value={newUsuarioFilial.ddd}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'10%' }}
            />
            <TextField 
            label="Telefone" 
            name="telefone" 
            value={newUsuarioFilial.telefone}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'30%' }}
            />
    </div>
   <div>
  <FormControl variant="outlined" style={{ marginRight: '20px', width:'20%'}}>
    <InputLabel>Perfil</InputLabel>
    <Select
        label="Perfis"
        name="id_perfil"
        value={newUsuarioFilial.id_perfil}
        onChange={handleChange}
    >
        <MenuItem value="">
        <em>--</em>
        </MenuItem>
        {
            perfisOptions.map(c=>(
                <MenuItem value={c.id}>{c.nome}</MenuItem>
            ))

        }
    </Select>
    </FormControl>
   <TextField 
            label="Senha" 
            name="senha" 
            value={newUsuarioFilial.senha}
            onChange={handleChange}
            variant="outlined"
            style={{marginBottom: '20px', marginRight:'20px', width:'30%' }}
            />
   </div>
    
        <Button type="submit" variant="contained" color="primary" style={{marginBottom: '20px', marginTop:'10px'}}>
          Adicionar
        </Button>
      </form>
      <Divider />
      <h2>Lista de Filiais</h2>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
            <TableCell>ID</TableCell>
              <TableCell>Nome</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>CPF</TableCell>
              <TableCell>DDD</TableCell>
              <TableCell>Ações</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              console.log(usuariosfiliais,'<----usuarios')
            }
            {
            
            usuariosfiliais.map((usuario) => (
              <TableRow key={usuario.id}>
                <TableCell>{usuario.id}</TableCell>
                <TableCell>{usuario.nome}</TableCell>
                <TableCell>{usuario.email}</TableCell>
                <TableCell>{usuario.cpf}</TableCell>
                <TableCell>{usuario.ddd}-{usuario.telefone}</TableCell>
                
                <TableCell>
                  <Button
                    color="primary"
                    onClick={() => {
                      setOpenEditDialog(true); 
                      setSelectedUsuarioFilial(usuario);
                      setSelectedFilialModal(usuario.id_filial);
                      setSelectedMantenedoraModal(usuario.id_mantenedora);
                    }}
                  >
                    Editar
                  </Button>

                  <Button 
                    
                    color="secondary"
                    onClick={() => {
                      setSelectedFilial(usuario);
                      setOpenDeleteDialog(true);
                    }}
                  >
                    Excluir
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

            <Dialog open={openDeleteDialog} onClose={() => setOpenDeleteDialog(false)}>
            <DialogTitle>Excluir Filial</DialogTitle>
            <DialogContent>
                <DialogContentText>
                Você tem certeza que deseja excluir a filial {selectedFilial?.nome_empresarial}?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setOpenDeleteDialog(false)}>Cancelar</Button>
                <Button onClick={() => deleteFilial(selectedFilial.id)} color="secondary">Excluir</Button>
            </DialogActions>
            </Dialog>



            <Dialog open={openEditDialog} onClose={() => setOpenEditDialog(false)}   PaperProps={{ sx: { width: "100%", height: "80%" } }}>
            <DialogTitle>Editar Usuário</DialogTitle>
            <DialogContent >
                <Divider />
                <h3>Dados do Usuário</h3>
                <h2>Selecione a Mantenedora e Filial</h2>
                <FormControl variant="outlined" style={{marginBottom: '20px', width:'200px', marginRight:'15px'}}>
                  <InputLabel>Mantenedora</InputLabel>
                  <Select
                    label="Mantenedora da filial"
                    name="id_mantenedora"
                    value={selectedMantenedoraModal || ''}
                    onChange={handleEditChange}
                    
                  >
                    <MenuItem value="">
                      <em>--</em>
                    </MenuItem>
                    {
                        mantenedorasOptions.map(m=>(
                            <MenuItem value={m.id}>{m.nome_empresarial}</MenuItem>
                        ))
                    }
                  </Select>
                </FormControl>
                {
                //console.log(selectedFilialModal,'<---------filial selecionada')
                }
                <FormControl variant="outlined" style={{marginBottom: '20px', width:'200px', marginRight:'15px'}}>
                  <InputLabel>Filial</InputLabel>
                  <Select
                    label="Filial"
                    name="id_filial"
                    value={selectedFilialModal || ''}
                    onChange={handleEditChange}
                  >
                    <MenuItem value="">
                      <em>--</em>
                    </MenuItem>
                    {
                        filiaisOptions.map(m=>(
                            <MenuItem value={m.id}>{m.nome_empresarial}</MenuItem>
                        ))
                    }
                  </Select>
                </FormControl>

                <div>
                    <TextField 
                    label="Nome" 
                    name="nome"
                    value={selectedUsuarioFilial?.nome || ''} 
                    onChange={handleEditChange}
                    style={{marginBottom: '20px', marginRight:'20px', width:'100%' }}
                    margin="normal"
                    />
                    <TextField 
                    label="CPF" 
                    name="cpf"
                    value={selectedUsuarioFilial?.cpf || ''}
                    onChange={handleEditChange}
                    style={{marginBottom: '20px', marginRight:'20px', width:'100%' }}
                    margin="normal"
                    />
                </div>
                <div>
                <TextField 
                label="Email" 
                name="email"
                value={selectedUsuarioFilial?.email || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', width:'48%' }}
                margin="normal"
                />
      
                <TextField 
                label="DDD" 
                name="ddd"
                value={selectedUsuarioFilial?.ddd || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', marginRight:'20px', width:'70%' }}
                margin="normal"
                />
                <TextField 
                label="Telefone" 
                name="telefone"
                value={selectedUsuarioFilial?.telefone || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', width:'25%' }}
                margin="normal"
                />
                </div>
  
              
               
                <div>
                <FormControl variant="outlined" style={{marginBottom: '20px', marginTop: '5px', width:'48%'}}>
                <InputLabel>Perfil</InputLabel>
                <Select
                    label="Perfis"
                    name="id_perfil"
                    value={selectedUsuarioFilial?.id_perfil}
                    onChange={handleEditChange}
                >
                    <MenuItem value="">
                    <em>--</em>
                    </MenuItem>
                    {
                        perfisOptions.map(c=>(
                            <MenuItem value={c.id}>{c.nome}</MenuItem>
                        ))

                    }
                </Select>
                </FormControl>
                <TextField 
                label="Senha" 
                name="senha"
                value={selectedUsuarioFilial?.senha || ''}
                onChange={handleEditChange}
                style={{marginBottom: '20px', marginTop: '5px',marginRight:'20px', width:'47%' }}
                margin="normal"
                />
                </div>
               

            </DialogContent>
            <DialogActions>
                <Button onClick={() => setOpenEditDialog(false)}>Cancelar</Button>
                <Button onClick={updateUsuarioFilial} color="primary">Atualizar</Button>
            </DialogActions>
            </Dialog>
      
    </div>
  );
};

export default UsuarioFilialApp;
