import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import CategoryList from "./CategoryList"
import CategoryListAct from "./CategoryListAct"
import CategoryRegister from "./CategoryRegister";
//import ProductList from "./ProductList";
import ProductListAct from "./ProductListAct";
import MantenedoraListAct from "./MantenedoraListAct";
import FilialListAct from "./FilialListAct";
import ComboListAct from "./ComboListAct";
import MedidasListAct from "./MedidasListAct";
import UsuariosFilialListAct from "./UsuariosFilialListAct";
import ProductRegister from "./ProductRegister";
import { Container } from "@mui/material";
import { Category, InsertDriveFile, Inventory, ViewList, Addchart } from '@mui/icons-material';
//import AppSettingsAltIcon from '@mui/icons-material/AppSettingsAlt';

const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  }),
);

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));

export default function Menu() {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [visivel, setVisivel]= React.useState()

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  function selectComponent(){
    if(visivel===1){
      return <ProductListAct texto={'Produtos'} />
    }
    else if(visivel===2){
      return <CategoryListAct texto={'Categorias'} />
      }
    else if(visivel===3){
      return <MantenedoraListAct texto={'Mantenedoras'} />
    }
    else if(visivel===4){
      return <FilialListAct texto={'Filiais'} />
    }
    else if(visivel===5){
    return <MedidasListAct texto={'Medidas'} />
    }
    else if(visivel===6){
      return <ComboListAct texto={'Combos'} />
      }
    else if(visivel===7){
      return <UsuariosFilialListAct texto={'Lista de Usuários por Filial'} />
      }
    else{
      return <CategoryList texto={'Lista de Categorias'} />
    }
  }

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar position="fixed" open={open}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{ mr: 2, ...(open && { display: 'none' }) }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div">
            Gerenciador de Produtos
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
        variant="persistent"
        anchor="left"
        open={open}
      >
        <DrawerHeader>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List>
        <ListItem onClick={()=>setVisivel(1)} key={1} disablePadding>
                <ListItemButton>
                <ListItemIcon>
                <Addchart />
                </ListItemIcon>
                <ListItemText primary={"Produtos"} />
                </ListItemButton>
        </ListItem>
        <ListItem onClick={()=>setVisivel(2)} key={2} disablePadding>
                <ListItemButton>
                <ListItemIcon>
                <Addchart />
                </ListItemIcon>
                <ListItemText primary={"Categorias"} />
                </ListItemButton>
        </ListItem>

            <ListItem onClick={()=>setVisivel(3)} key={3} disablePadding>
                <ListItemButton>
                <ListItemIcon>
                <Inventory />
                </ListItemIcon>
                <ListItemText primary={"Mantenedoras"} />
                </ListItemButton>
            </ListItem>
            <ListItem onClick={()=>setVisivel(4)} key={4} disablePadding>
                <ListItemButton>
                <ListItemIcon>
                <Category />
                </ListItemIcon>
                <ListItemText primary={"Filiais"} />
                </ListItemButton>
            </ListItem>
            <ListItem onClick={()=>setVisivel(5)} key={5} disablePadding>
                <ListItemButton>
                <ListItemIcon>
                <ViewList />
                </ListItemIcon>
                <ListItemText primary={"Medidas"} />
                </ListItemButton>
            </ListItem>
            <ListItem onClick={()=>setVisivel(6)} key={6} disablePadding>
                <ListItemButton>
                <ListItemIcon>
                <ViewList />
                </ListItemIcon>
                <ListItemText primary={"Combos"} />
                </ListItemButton>
            </ListItem>
            <ListItem onClick={()=>setVisivel(7)} key={7} disablePadding>
                <ListItemButton>
                <ListItemIcon>
                <ViewList />
                </ListItemIcon>
                <ListItemText primary={"Usuários Por Filial"} />
                </ListItemButton>
            </ListItem>
        </List>

      </Drawer>
      <Main open={open}>
        <DrawerHeader />
        <Container>
            {selectComponent()}
        </Container>
      </Main>
    </Box>
  );
}