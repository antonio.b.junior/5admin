import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { TextField, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import {  TableContainer,Table, TableHead, TableRow, TableCell, TableBody, Paper } from '@mui/material'

const CategoriaApp = () => {
  const [categorias, setCategorias] = useState([]);
  const [newCategoria, setNewCategoria] = useState({ nome: "", descricao: "" });
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [openEditDialog, setOpenEditDialog] = useState(false);
  const [selectedCategoria, setSelectedCategoria] = useState(null);

  const fetchCategorias = async () => {
    const response = await axios.get("http://localhost:3001/listarcategorias");
    setCategorias(response.data.categorias);
    console.log(response.data);
  };

  useEffect(() => {
    fetchCategorias();
  }, []);

  const addCategoria = async (e) => {
    e.preventDefault();
    const response = await axios.post("http://localhost:3001/salvarcategoria", newCategoria);
    setCategorias([...categorias, response.data]);
    setNewCategoria({ nome: "", descricao: "" });
    fetchCategorias();  // Atualiza a lista de categorias
  };

  const deleteCategoria = async (id) => {
    await axios.delete(`http://localhost:3001/excluircategoria/${id}`);
    setCategorias(categorias.filter(categoria => categoria.id !== id));
    setOpenDeleteDialog(false);
    fetchCategorias();  // Atualiza a lista de categorias
  };

  const updateCategoria = async () => {
    const response = await axios.put(`http://localhost:3001/updcategoria/${selectedCategoria.id}`, selectedCategoria);
    setCategorias(categorias.map(categoria => categoria.id === selectedCategoria.id ? response.data : categoria));
    setOpenEditDialog(false);
    fetchCategorias();  // Atualiza a lista de categorias
  };

  const handleChange = (e) => {
    setNewCategoria({ ...newCategoria, [e.target.name]: e.target.value });
  };

  const handleEditChange = (e) => {
    setSelectedCategoria({ ...selectedCategoria, [e.target.name]: e.target.value });
  };

  return (
    <div>
      <h1>Categorias</h1>
      <h2>Adicionar nova categoria</h2>
      <form onSubmit={addCategoria}>
        <TextField 
          label="Nome da categoria" 
          name="nome" 
          value={newCategoria.nome}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px'}}
        />
        <TextField 
          label="Descrição da categoria" 
          name="descricao" 
          value={newCategoria.descricao}
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom: '20px', marginRight:'20px'}}
        />
        <Button type="submit" variant="contained" color="primary" style={{marginTop:'10px'}}>
          Adicionar categoria
        </Button>
      </form>
      <h2>Categorias Cadastradas</h2>
      <TableContainer component={Paper}>
<Table sx={{ minWidth: 650 }} aria-label="simple table">
  <TableHead>
    <TableRow>
      <TableCell>ID</TableCell>
      <TableCell>Nome</TableCell>
      <TableCell>Descrição</TableCell>
      <TableCell>Ações</TableCell>
    </TableRow>
  </TableHead>
  <TableBody>
    {categorias && categorias.map((categoria) => (
      <TableRow
        key={categoria.id}
        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
      >
        <TableCell component="th" scope="row">{categoria.id}</TableCell>
        <TableCell component="th" scope="row">{categoria.nome}</TableCell>
        <TableCell>{categoria.descricao}</TableCell>
        <TableCell align="right">
            <Button color="primary" onClick={() => {setOpenEditDialog(true); setSelectedCategoria(categoria);}}>Editar</Button>
            <Button color="secondary" onClick={() => {setOpenDeleteDialog(true); setSelectedCategoria(categoria);}}>Excluir</Button>
        </TableCell>
      </TableRow>
    ))}
  </TableBody>
</Table>
</TableContainer>

      <Dialog open={openDeleteDialog} onClose={() => setOpenDeleteDialog(false)}>
        <DialogTitle>Excluir Categoria</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Você tem certeza que deseja excluir a categoria {selectedCategoria?.nome}?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpenDeleteDialog(false)}>Cancelar</Button>
          <Button onClick={() => deleteCategoria(selectedCategoria.id)} color="secondary">Excluir</Button>
        </DialogActions>
      </Dialog>

      <Dialog open={openEditDialog} onClose={() => setOpenEditDialog(false)}>
        <DialogTitle>Editar Categoria</DialogTitle>
        <DialogContent>
          <TextField 
            label="Nome da categoria" 
            name="nome"
            value={selectedCategoria?.nome || ''}
            onChange={handleEditChange}
            fullWidth
            margin="normal"
          />
          <TextField 
            label="Descrição da categoria" 
            name="descricao"
            value={selectedCategoria?.descricao || ''}
            onChange={handleEditChange}
            fullWidth
            margin="normal"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpenEditDialog(false)}>Cancelar</Button>
          <Button onClick={updateCategoria} color="primary">Atualizar</Button>
        </DialogActions>
      </Dialog>

    </div>
  );
};

export default CategoriaApp;
